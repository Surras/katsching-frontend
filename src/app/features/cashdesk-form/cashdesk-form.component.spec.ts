import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CashdeskFormComponent } from './cashdesk-form.component';

describe('CashdeskFormComponent', () => {
  let component: CashdeskFormComponent;
  let fixture: ComponentFixture<CashdeskFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CashdeskFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CashdeskFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
