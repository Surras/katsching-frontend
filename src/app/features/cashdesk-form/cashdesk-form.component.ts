import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BuyDialogComponent } from './buy-dialog/buy-dialog.component';

@Component({
  selector: 'app-cashdesk-form',
  templateUrl: './cashdesk-form.component.html',
  styleUrls: ['./cashdesk-form.component.scss'],
})
export class CashdeskFormComponent implements OnInit {
  constructor(public dialog: MatDialog) {}

  ngOnInit(): void {}

  openBuyDialog() {
    const dialogRef = this.dialog.open(BuyDialogComponent);

    dialogRef.afterClosed().subscribe((result) => {
      console.log(`Dialog result: ${result}`);
    });
  }
}
